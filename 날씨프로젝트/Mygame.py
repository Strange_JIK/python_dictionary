import game_framework
import urllib.request
from tkinter import *
import json
import  smtplib
from tkinter import font
from tkinter import Tk, ttk, StringVar,messagebox
from email.mime.text import  MIMEText
from urllib.request import Request
from urllib.request import urlopen
from urllib.parse   import quote_plus
from urllib.parse import urlencode
import  xml.etree.ElementTree as ET

name = "Main"

def enter():
    global city
    pass

def exit():
    pass

def ShowCityInfo(text,text2):
    URL = 'http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnRltmMesureDnsty'
    sidoName = text2

    queryParams = '?' + urlencode(
        {quote_plus(
            'ServiceKey'): '3VBoDPGPgJs5RX5aeKkM/aR9pe/zaMePqelfkZcIzEM9w+FHrMEAXR0cKMlMxK8YvBKGr1vbvaTwz3+dULIwvQ==',
         quote_plus('numOfRows'): '10', quote_plus('pageNo'): '1',
         quote_plus('sidoName'): sidoName, quote_plus('ver'): '1.3'})

    request = Request(URL + queryParams)
    request.get_method = lambda: 'GET'
    response_body = urlopen(request).read()
    filename = "mise.xml"
    f = open(filename, "wb")
    f.write(response_body)
    f.close()

    tree = ET.parse(filename)
    root = tree.getroot()


    url='http://api.openweathermap.org/data/2.5/weather?q='+text+'&mode=json&APPID=f2a4cd151d30cad7c8721aa75cf0d078'
    data = urllib.request.urlopen(url).read()
    j = json.loads(data)

    print(j)

    sunny_img= PhotoImage(file='Sunny.png')
    snowy_img= PhotoImage(file="Snowy.png")
    cludy_img=PhotoImage(file="Wind.png")
    rain_img=PhotoImage(file="Rain.png")

    weatherInfo = j['weather'][0]['main']
    print(text+" is "+weatherInfo)
    print("최고기온 : "+str(j['main']['temp_max']-273.15)+".c")
    print("최저기온 : "+str(j['main']['temp_min']-273.15)+".c")
    print("경도 : "+str(j['coord']['lat']))
    print("위도 : " + str(j['coord']['lon']))
    print("풍속 : " + str(j['wind']['speed'])+"m/s")
    print("습도 : "+str(j['main']['humidity']))
    window = Toplevel()
    window.title(text)
    window.geometry('400x500+10+10')  # width x height + 세로격자+가로격자
    l1 = Label(window,text="최고기온 : "+str(j['main']['temp_max']-273.15)+".c")
    l2 = Label(window,text="최저기온 : "+str(j['main']['temp_min']-273.15)+".c")
    l3 = Label(window,text="경   도 : "+str(j['coord']['lat']))
    l4 = Label(window,text="위   도 : "+ str(j['coord']['lon']))
    l5 = Label(window,text="풍   속 : "+ str(j['wind']['speed'])+"m/s")
    l6 = Label(window,text="습   도 : "+str(j['main']['humidity']))

    if weatherInfo=="Clouds":
        li = Label(window,image = cludy_img)
    elif weatherInfo=="Clear":
        li = Label(window,image=sunny_img)
    elif weatherInfo=="Rain":
        li = Label(window,image=rain_img)
    elif weatherInfo=="Snow":
        li= Label(window,image=snowy_img)
    elif weatherInfo=="Mist":
        li = Label(window,image=cludy_img)
    else:
        li = Label(window,image=cludy_img)
    e1 = Entry(window)
    l1.grid(row = 0,column = 0)
    l2.grid(row = 1,column = 0)
    l3.grid(row = 2,column = 0)
    l4.grid(row = 3,column = 0)
    l5.grid(row = 4,column = 0)
    l6.grid(row = 5,column = 0)
    li.grid(row = 2,column = 2)
    e1.grid(row=6,column =0)
    Dosilist = []
    coValueList = []
    pm25ValueList = []
    so2GradeList = []
    no2GradeList = []

    for a in root.findall('.//item'):
        dic = {"station": a.findtext("stationName"), "Date": a.findtext("dataTime"), "coValue": a.findtext("coValue"),
               "pm25Value": a.findtext("pm25Value"), "so2Grade": a.findtext("so2Grade"),
               "no2Grade": a.findtext("no2Grade")}
        Dosilist.append(a.findtext("stationName"))
        coValueList.append(a.findtext("coValue"))
        pm25ValueList.append(a.findtext("pm25Value"))
        so2GradeList.append(a.findtext("so2Grade"))
        no2GradeList.append(a.findtext("no2Grade"))
    global countryVar
    countryVar = StringVar()
    countryCombo = ttk.Combobox(window, textvariable=countryVar)

    countryCombo['values'] = Dosilist
    countryCombo.grid(row=8, column=0, padx=5, pady=5, ipady=2, sticky=W)
    global l7
    l7 = Label(window, text="대기 오염 정보 : ")
    l7.grid(row=7, column=0)

    # 리스트박스안에서 어떤녀석이 들어있는지만 알아서 그 정보를 뿌려주면 끝이다.
    button1 = Button(window, text="메일 보내기", command=lambda t=text: sendMail('nth0310@gmail.com', e1.get(),
                                                                            text + "\n최고기온 : " + str(j['main'][
                                                                                                         'temp_max'] - 273.15) + ".c\n" + "최저기온 : " + str(
                                                                                j['main'][
                                                                                    'temp_min'] - 273.15) + ".c\n" + "경   도 : " + str(
                                                                                j['coord']['lat']) + "\n위   도 : " + str(
                                                                                j['coord']['lon']) + "\n풍   속 : " + str(
                                                                                j['wind'][
                                                                                    'speed']) + "m/s" + "\n습   도 : " + str(
                                                                                j['main']['humidity'])+"\n------대기 오염 정보------ \n"
        + "일산화탄소 : " + coValueList[0] + "\n"
        + "미세먼지농도 : " + pm25ValueList[0] + "\n"
        + "이산화질소농도 : " + no2GradeList[0] + "\n"
        + "아황산가스농도 : " + so2GradeList[0]))
    button1.grid(row=6, column=1)

    button2 = Button(window, text="확인", command=lambda t=countryCombo.current(): change_text(
        "------대기 오염 정보------ \n"
        + "일산화탄소 : " + coValueList[t] + "\n"
        + "미세먼지농도 : " + pm25ValueList[t] + "\n"
        + "이산화질소농도 : " + no2GradeList[t] + "\n"
        + "아황산가스농도 : " + so2GradeList[t],
        pm25ValueList[t]
    ))
    button2.grid(row=8, column=1)
    window.mainloop()

def change_text(ctext, mise):
    good = PhotoImage(file="good.png")
    bad = PhotoImage(file="bad.png")
    verybad = PhotoImage(file="verybad.png")
    global countryVar
    global window
    print(countryVar.get())
    global l7

    l7.config(text=ctext)
    if int(mise)<80:
       l7.config(image=good,compound='bottom')
    elif int(mise)>=80 and int(mise)<150:
        l7.config(image=bad, compound='bottom')
    elif int(mise)>=150:
        l7.config(image=verybad, compound='bottom')
    window.mainloop()

def sendMail(me, you, msg):
    smtp = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    smtp.login(me, 'supoudodmwburakq')
    msg = MIMEText(msg)
    msg['Subject'] = '날씨정보'
    smtp.sendmail(me, you, msg.as_string())
    smtp.quit()
    messagebox.showinfo("Mail-translated", you+"로 메일을 전송하였습니다.")

def run():
    global window
    window = Tk()
    window.title('기상정보')
    window.geometry('690x900')  # width x height + 가로격자+세로격자

    map = 'map.png'
    img = PhotoImage(file=map)

    map_label = Label(window, image=img)
    map_label.pack()

    button1 = Button(window, text="서울",command=lambda t="Seoul": ShowCityInfo(t,"서울"))
    button1.place(x=326, y=442)

    button2 = Button(window, text='인천',command=lambda t="Incheon": ShowCityInfo(t,"인천"))
    button2.place(x=280, y=471)

    button3 = Button(window, text='대전',command=lambda t="Daejeon": ShowCityInfo(t,"대전"))
    button3.place(x=358, y=571)

    button4 = Button(window, text='대구',command=lambda t="Daegu": ShowCityInfo(t,"대구"))
    button4.place(x=460, y=619)

    button5 = Button(window, text='광주',command=lambda t="Kwangju": ShowCityInfo(t,"광주"))
    button5.place(x=314, y=694)

    button6 = Button(window, text='울산',command=lambda t="Ulsan": ShowCityInfo(t,"울산"))
    button6.place(x=520, y=652)

    button7 = Button(window, text='부산',command=lambda t="Busan": ShowCityInfo(t,"부산"))
    button7.place(x=503, y=692)

    button8 = Button(window, text='제주',command=lambda t="Jeju": ShowCityInfo(t,"제주"))
    button8.place(x=246, y=872)

    button10 = Button(window, text='경기',command=lambda t="Ansan-si": ShowCityInfo(t,"경기"))
    button10.place(x=336, y=485)

    button11 = Button(window, text='강원',command=lambda t="Wonju-si": ShowCityInfo(t,"강원"))
    button11.place(x=432, y=432)

    button12 = Button(window, text='충북',command=lambda t="Boryeong-si": ShowCityInfo(t,"충북"))
    button12.place(x=304, y=565)

    button13 = Button(window, text='충남',command=lambda t="Cheonan-si": ShowCityInfo(t,"충남"))
    button13.place(x=370, y=540)

    button14 = Button(window, text='전북',command=lambda t="Iksan-si": ShowCityInfo(t,"전북"))
    button14.place(x=308, y=646)

    button15 = Button(window, text='전남',command=lambda t="Seo-myeon": ShowCityInfo(t,"전남"))
    button15.place(x=290, y=751)

    button16 = Button(window, text='경북',command=lambda t="Gumi": ShowCityInfo(t,"경북"))
    button16.place(x=463, y=572)

    button17 = Button(window, text='경남',command=lambda t="Changwon": ShowCityInfo(t,"경남"))
    button17.place(x=412, y=685)




    window.mainloop()


def pause():
    pass


def resume():
    pass







